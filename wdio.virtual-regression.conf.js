var path = require('path');
var VisualRegressionCompare = require('wdio-visual-regression-service/compare');

function getScreenshotName(basePath) {
  return function(context) {
    var type = context.type;
    var testName = context.test.title;
    var browserVersion = parseInt(context.browser.version, 10);
    var browserName = context.browser.name;
    var browserWidth = context.meta.width;

    return path.join(basePath, `${testName}_${type}_${browserName}_${browserVersion}_${browserWidth}.png`);
  }
}

exports.config = {
  specs: [
    __dirname + '/visual-specs/*.spec.js'
  ],

  capabilities: [{
    browserName: 'chrome'
  }],

  services: [
    'selenium-standalone',
    'visual-regression',
  ],

  visualRegression: {
    compare: new VisualRegressionCompare.LocalCompare({
      referenceName: getScreenshotName(path.join(process.cwd(), 'screenshots/reference')),
      screenshotName: getScreenshotName(path.join(process.cwd(), 'screenshots/screen')),
      diffName: getScreenshotName(path.join(process.cwd(), 'screenshots/diff')),
      misMatchTolerance: 0.01,
    }),
    viewportChangePause: 300,
    widths: [320, 480, 640, 1024],
    orientations: ['langscape', 'portrait']

  },

  loglevel: 'verbose',

  coloredLogs: true,

  screenShotPath: './errorShots/',

  baseUrl: 'http://localhost:5000/',

  framework: 'mocha',

  reporters: ['dot'],

  mochaOpts: {
    ui: 'bdd',
    compilers: ['js:babel-register'],
    timeout: 30000
  }
}
