exports.config = {
  specs: [
    __dirname + '/specs/*.spec.js'
  ],

  services: ['sauce'],
  user: process.env.SAUCE_USERNAME,
  key: process.env.SAUCE_ACCESS_KEY,
  sauceConnect: true,

  capabilities: [{
    browserName: 'chrome'
  }],

  loglevel: 'verbose',

  coloredLogs: true,

  screenShotPath: './errorShots/',

  baseUrl: 'https://ja.wikipedia.org/',

  framework: 'mocha',

  reporters: ['dot'],

  mochaOpts: {
    ui: 'bdd',
    timeout: 30000
  }
}
