exports.config = {
  specs: [
    __dirname + '/specs/*.spec.js'
  ],

  capabilities: [{
    browserName: 'chrome'
  }],

  loglevel: 'verbose',

  coloredLogs: true,

  screenShotPath: './errorShots/',

  baseUrl: 'https://ja.wikipedia.org/',

  framework: 'mocha',

  reporters: ['dot'],

  mochaOpts: {
    ui: 'bdd',
    timeout: 30000
  }
}
