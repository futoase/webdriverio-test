var expect = require('chai').expect;
var IndexPage = require('../pageobjects/index.page');

describe('auth form', function(){
  it('should deny access with wrong creds', function() {
    IndexPage.open();

    expect(IndexPage.title.isExisting()).to.be.equal(true);
  });
});
