var page = require('./page');

var indexPage = Object.create(page, {
  title: { get: function() { return $('//title'); } },

  open: { value: function() {
    page.open.call(this, '/');
  }}
});

module.exports = indexPage;
