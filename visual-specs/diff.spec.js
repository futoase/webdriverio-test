var expect = require('chai').expect;
var widths = [1024, 1600];

function assertDiff(results) {
  results.forEach((result) => assert.ok(result.isExactSameImage));
}

describe('diff test', function() {
  it ('should get diff image for index page', async function() {
    await browser.url('/');
    await browser.pause(2000);

    const reports = await browser.checkViewport({
      widths: widths
    });

    reports.forEach((result) =>
      expect(result.isExactSameImage).to.be.equal(false)
    );
  });
});
