var http = require('http');
var hostname = '127.0.0.1';
var port = '5000';

var server = http.createServer(function (req, res) {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.end(`<html><body><h1>Date Time</h1><p>${(new Date)}</p></body></html>`);
});

server.listen(port, hostname, function() {
  console.log('starting server...');
});
